<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Phone Book</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <!-- Styles -->
    <style>
        html, body {
            color: #636b6f;
            font-family: "Times New Roman";
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        main{
            width: 90%;
            margin: auto;
            text-align: center;
        }
        h1{
            margin-bottom: 60px;
            margin-top: 30px;
            font-weight: 600;
            text-align: center;
        }
        .search-create-container{
            display: flex;
            justify-content: space-between;
        }
        #form-container{
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .content {
            width: 40%;
            height: auto;
            border: 2px solid #b9c1cb;
            text-align: center;
            padding-top: 30px;
            padding-bottom: 30px;
            box-shadow: 4px 4px 5px 5px rgba(34, 60, 80, 0.5);
            margin-top: 60px;
        }
        .input-group_item-search{
            width: 30% !important;
        }
        .form-group{
            padding: 15px;
        }
        .form-control_item{
            width: 70%;
            height: 50px;
            margin: auto;
            box-shadow: none !important;
        }
        .btn-submit{
            margin-left: 40px;
            background-color: #5e7380 !important;
            border-color: #5e7380 !important;
        }
        span{
            margin-left: 40px;
            margin-bottom: 20px;
            font-weight: 600;
            color: #d00000;
        }
        button{
            border: none;
            background-color: white;
        }
        a{
            text-decoration: none;
            color: black;
        }
        .input-group-prepend{
            margin-left: 3px !important;
        }
    </style>
</head>
<body>
@yield("content")
</body>
</html>
