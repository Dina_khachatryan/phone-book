@extends("layouts.main")

@section("content")
    <div id="form-container">
        <form class="content" method="post" action="{{ route('createPhoneBookItemData') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <h2>Add phone book item</h2>
            <div class="content-form-groups">
                <div class="form-group">
                    <input type="text" class="form-control form-control_item" name="first_name" placeholder="First name">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control form-control_item" name="last_name" placeholder="Last name">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control form-control_item masked" pattern="^\+374(\s+)?[0-9]{2}(\s+)?[0-9]{2}?[0-9]{2}?[0-9]{2}$" name="phone_number" placeholder="e.g. +374 22 334455">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control form-control_item" name="country_code" placeholder="Country code">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control form-control_item" name="timezone_name" placeholder="Timezone">
                </div>
            </div>
            <div class="errors">
                @foreach ($errors->all() as $message)
                    <span>{{ $message }}</span>
                @endforeach
            </div>
            <button type="submit" name="submit" class="btn btn-outline-secondary">SUBMIT</button>
        </form>
    </div>
@endsection
