@extends("layouts.main")

@section("content")
    <div id="form-container">
        <form class="content" method="post" action="{{ route('updatePhoneBookItem', $item->id) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="_method" value="PUT">
            <h2>Update phone book item</h2>
            <div class="content-form-groups">
                <div class="form-group">
                    <label for="firstName">First name</label>
                    <input type="text" class="form-control form-control_item" name="first_name" value="{{$item->first_name}}">
                </div>
                <div class="form-group">
                    <label for="lastname">Last name</label>
                    <input type="text" class="form-control form-control_item" name="last_name" value="{{$item->last_name}}">
                </div>
                <div class="form-group">
                    <label for="phoneNumber">Phone number</label>
                    <input type="text" class="form-control form-control_item masked" pattern="^\+374(\s+)?[0-9]{2}(\s+)?[0-9]{2}?[0-9]{2}?[0-9]{2}$" name="phone_number" value="{{$item->phone_number}}">
                </div>
                <div class="form-group">
                    <label for="countryCode">Country code</label>
                    <input type="text" class="form-control form-control_item" name="country_code" value="{{$item->country_code}}">
                </div>
                <div class="form-group">
                    <label for="firstName">Timezone name</label>
                    <input type="text" class="form-control form-control_item" name="timezone_name" value="{{$item->timezone_name}}">
                </div>
            </div>
            <div class="errors">
                @foreach ($errors->all() as $message)
                    <span>{{ $message }}</span>
                @endforeach
            </div><br>
            <button type="submit" name="submit" class="btn btn-outline-secondary">SUBMIT</button>
        </form>
    </div>
@endsection

