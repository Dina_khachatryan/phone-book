<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneBookItem extends Model
{
       protected $fillable = [
         'first_name',
         'last_name',
         'phone_number',
         'country_code',
         'timezone_name'
       ];
}
