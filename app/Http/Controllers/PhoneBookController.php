<?php

namespace App\Http\Controllers;

use App\PhoneBookItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\URL;

class PhoneBookController extends Controller
{
    /**
     * Display a listing of the phone book items.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $phoneBookItems = PhoneBookItem::all();

        return view('phoneBook', compact('phoneBookItems'));
    }

    /**
     * Show the form for creating a new phone book item.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.create');
    }

    /**
     * Store a newly created phone book item in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'required|regex:/^\+374(\s+)?[0-9]{2}(\s+)?[0-9]{2}?[0-9]{2}?[0-9]{2}$/|min:9'
        ]);

        $firstName = $request->get('first_name');
        $lastName = $request->get('last_name');
        $phoneNumber = $request->get('phone_number');
        $countryCode = $request->get('country_code');
        $timezoneName = $request->get('timezone_name');

        /**
         * Validate country code
         */

        if ($countryCode) {
            try {
                $getCountryCodesRequest = Http::get("http://country.io/continent.json");
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(['There was an error while trying to find the country code, please try again in 5 minutes.']);
            }

            $getCountryCodesResponse = $getCountryCodesRequest->json();

            if(!in_array($countryCode, $getCountryCodesResponse)){
                return redirect()->back()->withErrors(['Country code does not exist.']);
            }
        }

        /**
         * Validate timezone name
         */

        if ($timezoneName) {
            try {
                $getTimezoneRequest = Http::get("http://worldtimeapi.org/api/timezone");
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(['There was an error while trying to find the country code, please try again in 5 minutes.']);
            }

            $getTimezoneResponse = $getTimezoneRequest->json();

            if(!in_array($timezoneName, $getTimezoneResponse)){
                return redirect()->back()->withErrors(['Timezone name does not exist.']);
            }
        }

        /**
         * Insert into DB Phone book item data
         */
        PhoneBookItem::create([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'phone_number' => $phoneNumber,
            'country_code' => $countryCode,
            'timezone_name' => $timezoneName,

        ]);

        return redirect('/');
    }

    /**
     * Show the form for editing the phone book item.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = PhoneBookItem::find($id);
        return view('forms.update', compact('item'));
    }

    /**
     * Update the phone book item in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'required|regex:/^\+374(\s+)?[0-9]{2}(\s+)?[0-9]{2}?[0-9]{2}?[0-9]{2}$/|min:9'
        ]);

        $item = PhoneBookItem::find($id);
        $item->first_name =  $request->get('first_name');
        $item->last_name = $request->get('last_name');
        $item->phone_number = $request->get('phone_number');
        $item->country_code = $request->get('country_code');
        $item->timezone_name = $request->get('timezone_name');

        /**
         * Validate country code
         */

        if ($item->country_code) {
            try {
                $getCountryCodesRequest = Http::get("http://country.io/continent.json");
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(['There was an error while trying to find the country code, please try again in 5 minutes.']);
            }

            $getCountryCodesResponse = $getCountryCodesRequest->json();

            if(!in_array($item->country_code, $getCountryCodesResponse)){
                return redirect()->back()->withErrors(['Country code does not exist.']);
            }
        }

        /**
         * Validate timezone name
         */

        if ($item->timezone_name) {
            try {
                $getTimezoneRequest = Http::get("http://worldtimeapi.org/api/timezone");
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(['There was an error while trying to find the country code, please try again in 5 minutes.']);
            }

            $getTimezoneResponse = $getTimezoneRequest->json();

            if(!in_array($item->timezone_name, $getTimezoneResponse)){
                return redirect()->back()->withErrors(['Timezone name does not exist.']);
            }
        }
        $item->save();

        return redirect('/')->with('success', 'Contact updated!');
    }

    /**
     * Remove the phone book item from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $phoneBookItem = PhoneBookItem::find($id);
        $phoneBookItem->delete();

        return redirect('/')->with('success', 'Contact deleted!');
    }

    /**
     * Search phone book item.
     */
    public function search(Request $request){
        $search = $request->get('search');
        $phoneBookItems = PhoneBookItem::where('first_name', 'like', '%' .$search. '%')
            ->orWhere('last_name', 'like', '%' .$search. '%')
            ->paginate(5);

        return view('phoneBook', compact('phoneBookItems'));
    }
}
