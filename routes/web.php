<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PhoneBookController@index');
Route::get('/create-phone-book-item', 'PhoneBookController@create')->name('createPhoneBookItem');
Route::post('/store-phone-book-item', 'PhoneBookController@store')->name('createPhoneBookItemData');
Route::get('/{id}/edit-phone-book-item', 'PhoneBookController@edit')->name('editPhoneBookItem');
Route::put('/{id}/update-phone-book-item', 'PhoneBookController@update')->name('updatePhoneBookItem');
Route::get('/{id}/delete-phone-book-item', 'PhoneBookController@destroy')->name('deletePhoneBookItem');
Route::get('search', 'PhoneBookController@search')->name('search');


